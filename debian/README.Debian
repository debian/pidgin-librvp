This is librvp (pidgin-librvp under Debian, formerly gaim-librvp), a plugin for
Pidgin which implements support for the RVP protocol used by Microsoft Exchange
instant messaging systems.  This is not the same as the protocol used by MSN
Messenger -- if you're looking for that, it's natively included in the Pidgin
package.  The RVP/Exchange protocol is primarily used in private corporate IM
contexts.

To use the plugin to connect to an Exchange IM server, install the package (if
you're reading this, chances are you've done that part already), then
(re)start Pidgin.  From the 'Accounts' dialog, click 'Add', then pull the
'Protocol" popup menu down to the 'RVP' entry.  The authentication settings
are the same those used to connect to the Exchange server via NTLM.

Because RVP clients open a TCP port on which to listen for asynchronous event
notifications from the server, you may need to open one or more ports on your
host firewall to inbound TCP packets from the server.  You can pick a specific
port on which to listen by entering it in the "Fixed Port" setting of the
account properties dialog (expand the "Show more options" section.)  As of
0.7, librvp uses its own setting in preference to the listening port range in
the main Pidgin configuration.

librvp is derived in part from a series of patches to 0.x-era versions of
GAIM, by engineers at Lucent/Bell Labs.  Although it is the most complete and
least rough free implementation of the bug to date, it still has bugs.  When
reporting these, it's very helpful to reproduce the bug while running "pidgin
--debug", redirecting the output to a trace file, and including that file with
your report.


A now-expired IETF draft protocol specification was published describing RVP,
which can be found here:

  <http://www3.ietf.org/proceedings/99jul/I-D/draft-calsyn-rvp-01.txt>


$Id: README.Debian 656 2007-05-28 06:47:23Z aqua $


/* MD5 Digest Authentication.

   cribbed from LWP::Authen::Digest, the RFC, and auth.c in the jabber
   code. Moving out of the static RVP module so that others can use it
   if necessary.
*/

/*
 * Reverse-engineering reveals that the Microsoft client:
 * - supports MD5 and MD5-sess
 * - supports opaque
 * - may support stale, it at least doesn't abandon auth when it sees it
 * - only supports qop=auth (no auth-int, and definitely no missing qop)
 *
 * Anything it doesn't like causes it to revert to not authing at all.
 */
#include <glib.h>
#include <string.h>

/* Gaim includes */
#include <debug.h>

/* local include */
#include "digest_access_auth.h"

#ifndef NOMD5
/* shims for gcrypt */
#ifdef GCRYPT_MD5
typedef guint8 md5_byte_t;
#define md5_state_t gcry_md_hd_t
#define md5_init( handle ) gcry_md_open( handle, GCRY_MD_MD5, 0 )

static void md5_append( md5_state_t *handle, md5_byte_t *buffer, size_t length ) {
  gcry_md_write( *handle, buffer, length );
}

static void md5_finish( md5_state_t *handle, md5_byte_t *buffer ) {
  unsigned char *buf = gcry_md_read( *handle, 0 );
  bcopy( buf, buffer, 16 ); /* MD5 length == 16 */
  gcry_md_close( *handle );
}
#endif

gchar *get_auth_digest( GaimConnection *gc, gchar *method, gchar *uri,
                        gchar *header, gchar *user, gchar *pass ) {
  gchar **auth_param = NULL;
  gint i = 0;
  gchar *cnonce, *a1 = NULL, *a2, *retval = NULL, hexresp[33];
  md5_state_t md5;
  md5_byte_t ha2[16], response[16];
  GHashTable *params = g_hash_table_new( g_str_hash, g_str_equal );

  /* slightly naive, but seems to be acceptable for now. FIXME */
  auth_param = g_strsplit( &header[strlen( "Digest ")], ",", 0 );

  /* default algorithm */
  g_hash_table_replace( params, g_strdup( "algorithm" ), g_strdup( "MD5" ));

  /* find the nonce, etc. */
  for ( i = 0; auth_param[i]; i++ ) {
    gchar **bits;

    /* no leading/trailing spaces, thanks. */
    g_strstrip( auth_param[i] );

    bits = g_strsplit( auth_param[i], "=", 2 );

    /* clean up value by unquoting it FIXME */
    if ( bits[1][0] == '"' ) {
      memcpy( &bits[1][0], &bits[1][1],
              strlen( bits[1] ) - 1 );
      bits[1][strlen( bits[1] ) - 1] = '\0';
    }
    if ( bits[1][strlen( bits[1] ) - 1] == '"' ) {
      bits[1][strlen( bits[1] ) - 1 ] = '\0';
    }
    g_hash_table_replace( params, g_strdup( bits[0] ), g_strdup( bits[1] ));

    gaim_debug_misc( __FUNCTION__, "Found %s = %s\n", bits[0], bits[1] );

    g_strfreev( bits );
  }

  if ( g_hash_table_lookup( params, "nonce" ) == NULL ||
       g_hash_table_lookup( params, "realm" ) == NULL ) {
    gaim_connection_error( gc,
                           "Can't find one of nonce or realm in header" );
    return NULL;
  }

  cnonce = g_strdup_printf( "%x%u%x", g_random_int(), (int)time( NULL ),
                            g_random_int() );

  if ( !strcasecmp( g_hash_table_lookup( params, "algorithm" ), "MD5" )) {
    /* A1 = "username : realm : passwd"; */
    md5_init( &md5 );
    md5_append( &md5, (md5_byte_t *)user, strlen( user ));
    md5_append( &md5, (md5_byte_t *)":", 1 );
    md5_append( &md5, (md5_byte_t *)g_hash_table_lookup( params, "realm" ),
                strlen( g_hash_table_lookup( params, "realm" )));
    md5_append( &md5, (md5_byte_t *)":", 1 );
    md5_append( &md5, (md5_byte_t *)pass, strlen( pass ));
    a1 = g_malloc0( 16 + 1 );
    md5_finish( &md5, (md5_byte_t *)a1 );
    a1[16] = '\0';
  } else if ( !strcasecmp( g_hash_table_lookup( params, "algorithm" ),
                           "MD5-sess" )) {
    /* A1 = "H( username : realm : passwd ):nonce:cnonce"; */
    md5_byte_t a1_b[16];

    md5_init( &md5 );
    md5_append( &md5, (md5_byte_t *)user, strlen( user ));
    md5_append( &md5, (md5_byte_t *)":", 1 );
    md5_append( &md5, (md5_byte_t *)g_hash_table_lookup( params, "realm" ),
                strlen( g_hash_table_lookup( params, "realm" )));
    md5_append( &md5, (md5_byte_t *)":", 1 );
    md5_append( &md5, (md5_byte_t *)pass, strlen( pass ));
    md5_finish( &md5, a1_b );

    /* hexify */
    a1 = g_malloc0( 32 + strlen( g_hash_table_lookup( params, "nonce" )) +
                    strlen( cnonce ) + 1 );
    for ( i = 0; i < 16; i++ ) {
      sprintf( &a1[i * 2], "%02x", (guint8)a1_b[i] );
    }

    /* append nonce & cnonce */
    memcpy( &a1[32], g_hash_table_lookup( params, "nonce" ),
            strlen( g_hash_table_lookup( params, "nonce" )));
    memcpy( &a1[32 + strlen( g_hash_table_lookup( params, "nonce" ) )], cnonce,
            strlen( cnonce ));
    a1[32 + strlen( g_hash_table_lookup( params, "nonce" ) ) + strlen( cnonce )] = '\0';
  } else {
    gchar *msg = g_strdup_printf( "Server asked for unknown algorithm '%s'",
                                 (char *)g_hash_table_lookup( params,
                                                              "algorithm" ));
    gaim_connection_error( gc, msg );
    g_free( msg );
    return NULL;
  }

  if ( g_hash_table_lookup( params, "qop" ) != NULL &&
       ( !strcmp( g_hash_table_lookup( params, "qop" ), "auth" ) ||
         !strcmp( g_hash_table_lookup( params, "qop" ), "auth-int" ))) {
    if ( !strcmp( g_hash_table_lookup( params, "qop" ), "auth" )) {
      /* A2 = method : uri */
      a2 = g_strdup_printf( "%s:%s%s", method, uri[0] == '/' ? "" : "/", uri );
    } else {
      /* we don't support auth-int just now */
      /* A2 = method : uri : content */
      a2 = g_strdup_printf( "%s:%s%s:%s", method, uri[0] == '/' ? "" : "/",
                            uri, "HEXME(XXX)" );
    }
  } else {
    /* A2 = method : uri */
    a2 = g_strdup_printf( "%s:%s%s", method, uri[0] == '/' ? "" : "/", uri );
  }

  /* MD5-ify a2 */
  md5_init( &md5 );
  md5_append( &md5, (md5_byte_t *)a2, strlen( a2 ));
  md5_finish( &md5, ha2 );

  /* now gob it all together appropriate to the qop parameter */
  if ( g_hash_table_lookup( params, "qop" ) != NULL &&
       ( !strcmp( g_hash_table_lookup( params, "qop" ), "auth" ) ||
         !strcmp( g_hash_table_lookup( params, "qop" ), "auth-int" ))) {
    /* request-digest = KD(H(A1), nonce : nc : cnonce : qop : H(A2) */
    md5_init( &md5 );

    /* H(A1) */
    for ( i = 0; i < strlen( a1 ); i++ ) {
      gchar *hex = g_strdup_printf( "%02x", (guint8)a1[i] );
      md5_append( &md5, (md5_byte_t *)hex, 2 );
      g_free( hex );
    }

    /* :nonce */
    md5_append( &md5, (md5_byte_t *)":", 1 );
    md5_append( &md5, (md5_byte_t *)g_hash_table_lookup( params, "nonce" ), strlen( g_hash_table_lookup( params, "nonce" ) ));

    /* :nc */
    md5_append( &md5, (md5_byte_t *)":", 1 );
    md5_append( &md5, (md5_byte_t *)"00000001", strlen( "00000001" ));


    /* :cnonce */
    md5_append( &md5, (md5_byte_t *)":", 1 );
    md5_append( &md5, (md5_byte_t *)cnonce, strlen( cnonce ));

    /* :qop */
    md5_append( &md5, (md5_byte_t *)":", 1 );
    md5_append( &md5, (md5_byte_t *)g_hash_table_lookup( params, "qop" ), strlen( g_hash_table_lookup( params, "qop" ) ));

    /* : */
    md5_append( &md5, (md5_byte_t *)":", 1 );

    /* H(A2) */
    for ( i = 0; i < 16; i++ ) {
      gchar *hex = g_strdup_printf( "%02x", (guint8)ha2[i] );
      md5_append( &md5, (md5_byte_t *)hex, 2 );
      g_free( hex );
    }
    md5_finish( &md5, response );

    /* turn response into hex */
    for ( i = 0; i < 16; i++ ) {
      sprintf( &hexresp[i * 2], "%02x", (guint8)response[i] );
    }
    hexresp[32] = '\0';

    retval = g_strdup_printf( "Digest "
                              "username=\"%s\", "
                              "realm=\"%s\", "
                              "qop=\"%s\", "
                              "algorithm=\"%s\", "
                              "uri=\"%s%s\", "
                              "nonce=\"%s\", "
                              "nc=00000001, "
                              "cnonce=\"%s\", "
                              "response=\"%s\"",
                              user,
                              (char *)g_hash_table_lookup( params, "realm" ),
                              (char *)g_hash_table_lookup( params, "qop" ),
                              (char *)g_hash_table_lookup( params, "algorithm" ),
                              uri[0] == '/' ? "" : "/", uri,
                              (char *)g_hash_table_lookup( params, "nonce" ),
                              cnonce,
                              hexresp );
  } else {
    /* request-digest = KD(H(A1), nonce : H(A2) */
    md5_init( &md5 );

    /* H(A1) */
    for ( i = 0; i < 16; i++ ) {
      gchar *hex = g_strdup_printf( "%02x", (guint8)a1[i] );
      md5_append( &md5, (md5_byte_t *)hex, 2 );
      g_free( hex );
    }

    /* :nonce: */
    md5_append( &md5, (md5_byte_t *)":", 1 );
    md5_append( &md5, (md5_byte_t *)g_hash_table_lookup( params, "nonce" ), strlen( g_hash_table_lookup( params, "nonce" ) ));
    md5_append( &md5, (md5_byte_t *)":", 1 );

    /* H(A2) */
    for ( i = 0; i < 16; i++ ) {
      gchar *hex = g_strdup_printf( "%02x", (guint8)ha2[i] );
      md5_append( &md5, (md5_byte_t *)hex, 2 );
      g_free( hex );
    }
    md5_finish( &md5, response );

    /* turn response into hex */
    for ( i = 0; i < 16; i++ ) {
      sprintf( &hexresp[i * 2], "%02x", (guint8)response[i] );
    }
    hexresp[32] = '\0';

    retval = g_strdup_printf( "Digest "
                              "username=\"%s\", "
                              "realm=\"%s\", "
                              "uri=\"%s%s\", "
                              "nonce=\"%s\", "
                              "response=\"%s\"",
                              user,
                              (char *)g_hash_table_lookup( params, "realm" ),
                              uri[0] == '/' ? "" : "/", uri,
                              (char *)g_hash_table_lookup( params, "nonce" ),
                              hexresp );
  }

  if ( auth_param != NULL ) {
    g_strfreev( auth_param );
  }

  if ( a1 != NULL ) { g_free( a1 ); }

  return retval;
}
#endif

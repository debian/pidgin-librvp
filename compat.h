/* Let's do the compatibility dance!
   My aim here is that the 'default' code should be Gaim 2 compatible,
   and that any hacks to enable compatibility - macros, etc. - should
   be to make gaim-2 bits appear palatable to gaim-1.
*/
#ifndef _RVP_COMPAT_H
#define _RVP_COMPAT_H

#if GAIM_MAJOR_VERSION < 2
/* this is actually pretty sane */
#define GAIM_CONV_TYPE_CHAT GAIM_CONV_CHAT
#define GaimMenuAction GaimBlistNodeAction
#define GaimMessageFlags GaimConvImFlags
#define gaim_menu_item_new( label, callback, thing1, thing2 ) \
  gaim_blist_node_action_new( label, callback, thing1 )
#define gaim_find_conv_with_account( type, recip, ac ) \
  gaim_find_conversation_with_account( recip, ac )

/* this is sort of silly, but allows me to handle function signature changes */
#define rvpleconst
typedef ssize_t rvpxrr_size;
typedef char rvpxrr_buf;
typedef GaimInputCondition proxy_connect_callback_unused;
typedef int rvp_st_ret;
typedef int rvp_typing;

/* wrapper for old versions */
#include <network.h>
gboolean rvp_network_listen_range( int low, int high, int type /* NOTUSED */,
                                   void (*callback)( int, gpointer ),
                                   gpointer data ) {
  int sockfd = gaim_network_listen_range( low, high );

  if ( sockfd == -1 ) {
    return FALSE;
  }

  callback( sockfd, data );

  return TRUE;
}

typedef char * GaimProxyConnectData;
typedef GaimInputFunction GaimProxyConnectFunction;
static GaimProxyConnectData gpcd_fake = "FAKER";

GaimProxyConnectData rvp_proxy_connect( void *handle, GaimAccount *account, const char *host, int port, GaimProxyConnectFunction connect_cb, gpointer data ) {
  int sockfd = gaim_proxy_connect( account, host, port, connect_cb, data );
  if ( sockfd == -1 ) {
    return NULL;
  }

  return gpcd_fake;
}

#include <internal.h> /* this is here for the gettext bits */
#else

#define rvp_network_listen_range gaim_network_listen_range
#define gaim_menu_item_new( label, callback, thing1, thing2 ) \
  gaim_menu_action_new( label, GAIM_CALLBACK(callback), thing1, thing2 )
#define gaim_find_conv_with_account( type, recip, ac ) \
  gaim_find_conversation_with_account( type, recip, ac )
#define rvp_proxy_connect gaim_proxy_connect

#define rvpleconst const
typedef gssize rvpxrr_size;
typedef guchar rvpxrr_buf;
typedef const gchar * proxy_connect_callback_unused;
typedef unsigned int rvp_st_ret;
typedef GaimTypingState rvp_typing;

/* ripped from internal.h */
#ifdef ENABLE_NLS
#  include <locale.h>
#  include <libintl.h>
#  define _(x) ((const char *)gettext(x))
#  ifdef gettext_noop
#    define N_(String) gettext_noop (String)
#  else
#    define N_(String) (String)
#  endif
#else
#  include <locale.h>
#  define N_(String) (String)
#  ifndef _
#    define _(x) ((const char *)x)
#  endif
#  define ngettext(Singular, Plural, Number) ((Number == 1) ? ((const char *)Singular) : ((const char *)Plural))
#endif

/* why isn't this exported ? */
#define BUF_LEN 2048

#endif

/* and because it wouldn't be a minor upgrade with a name change and a
   compat header if something didn't break SOMEWHERE... */
#ifdef PURPLE_MAJOR_VERSION
#undef gaim_request_file
#define gaim_request_file( handle, title, filename, savedialog, ok, cancel, data ) purple_request_file( handle, title, filename, savedialog, ok, cancel, NULL, NULL, NULL, data )
#define rvp_list_emblems rvp_list_emblems_purple
#else
#define rvp_list_emblems rvp_list_emblems_gaim
#endif

#endif
